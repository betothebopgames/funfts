# Page Directory

## Main 
The main page will start with an introduction and a preview of the Featured Gallery and will include links to:
 * Register as a new player
 * Sign in to player account
 * Find out more 'About' this site
 * View the Featured Gallery of fuNFTs
 * Support the developer on Ko-Fi

## Sign-In 
Page for signing in using player id and passkey.

## Registration 
Form to register as a new player.

## About
What is the fuNFT experience?
What are NFTs?
Check out the knowledge guide!

## Featured Gallery
Display several selected fuNFTs. Allow users to bid on them in auction.

## Player Hub
Shows the players game status and links to individual sections.
 * Messages
 * Personal gallery
 * funCoin mining
 * Private collection
 * Users followed
 
## Profile
Each players public facing page.
 * General info
 * Personal Gallery
 * Message section

## Manage fuNFTs
List fuNFTs in both personal gallery and private collection. Users can put fuNFTs on sale in their Personal Gallery.

## Mining Funcoin
Players can manage their funCoin miners which gradually gain them funCoin currency over time. Upgrades can be made to improve the mining power.

## NFT Creator
Players can design their own fuNFTs to add to their collection and sell to others.

## Admin Panel
This is used for moderating and managing the site with administrative permission.

