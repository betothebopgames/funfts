# Database Structure

## Users

* UserID - 6 digit random Base32 number, over 1 billion possibilites
* PassKeyHash - 20 digit random Base32 authentication key, hashed
* E-Mail - For recovery purposes.
* Display Name
* Last Login Date
* funCoin Balance
* funCoin Miner Level
* Moderation Status - Unverified, Verified, Banned
* (Later) Premium member status


## fuNFTs

* fuNFT ID - 4 digit sequential Base32 number
* Image data
  + Palette of 16 colors each 12 bits, 24 bytes
  + Pixels in 16 x 16 grid each 4 bits, 128 bytes
* Image Hash - to ensure uniqueness
* CreatorID - player who first created funft
* OwnerID - player who currently owns funft
* Status - in gallery, in storage, in auction, disabled
* Price in gallery
* Last selling price

## Messages
* Message ID
* Sender ID
* Recipient ID
* Date
* Message

## Auctions
* Auction ID
* fuNFT ID
* Current Asking price
* Start Date
* End Date

## Other possible tables
* Sale history
* Ban history
* FunCoin Mining dedicated
