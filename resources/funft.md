# FUNFT Market Game

## Summary

What is a (FUN)FT? These Fantastic Unique Nostalgic Fictional Treasures 
are small pieces of digital art that are "securely" stored on this 
server. They can be purchased with FUNcoins that you can earn right 
here on the site. Can you collect the most FUNFT's? More than your 
friends?

Back to reality, this is an online game poking a little fun at the idea 
of NFT's or Non-Fungible Tokens, the digital assets that some people invest a lot of money into. FUNFT's unlike real NFT's will not be stored on any blockchain tecchnology and are unlikely to be worth any real world value. Players will be able to earn digital currency on the site and then spend it on buying different FUNFT's or earn money by creating their own.

FUNFT's will more specifically be an image with a small resolution (16 x 16), a limited color palette (16 colors), which can be represented with encoded text (Base64 or Base32). Each one should be a very small size, 176 bytes, with these settings.

Players will earn FUNcoins by creating their own FUNFT's. They can put these FUNFT's on sale in their personal galleries. Sometimes random NPC's, non-player characters, will buy these FUNFT's. Also, contests can be held where players can submit their FUNFT's and win prizes. They can also get FUNcoins if other players buy their FUNFTs. The site will also have a Featured Gallery where everybody on the site can view and purchase these highly valued FUNFT's in online auction format.

## Technical Specs
Users
+ id
+ username
+ passcode
+ Personal FUNFT
+ Owned FUNFTs
+ On-Sale FUNFTs
+ FUNcoins

FUNFTs
+ id
+ content
+ owner id
+ Estimate value

Pages
+ User Galleries
+ Featured Gallery
+ FUNFT Editor
+ FUNFT Detail Page
+ Contest Page

## Tools
+ NextJS (Framework)
+ Two.js (Image Generator)
+ jscolor.js (Color picker)
+ [Drawing App tutorial](https://codepen.io/javascriptacademy-stash/pen/porpeoJ)
+ [PouchDB Tutorial](https://levelup.gitconnected.com/building-an-offline-website-app-with-react-next-js-and-pouchdb-that-syncs-a-deep-dive-d16b7cdbf06d)
