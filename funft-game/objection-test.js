'use strict';

const Knex = require('knex');
const knexConfig = require('./knexfile');

const { Model } = require('objection');
const { User } = require('./db/models/user');

// Initialize knex.
const knex = Knex(knexConfig.development);

// Bind all Models to the knex instance. You only
// need to do this once before you use any of
// your model classes.
Model.knex(knex);

async function main() {
  // Delete all persons from the db.
  await User.query().delete();

  // Insert one row to the database.
  await User.query().insert({
    name: 'Jennifer',
  });

  // Read all rows from the db.
  const users = await User.query();

  console.log(users);
}

main()
  .then(() => knex.destroy())
  .catch((err) => {
    console.error(err);
    return knex.destroy();
  });
