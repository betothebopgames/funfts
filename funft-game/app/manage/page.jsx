import { BigHeading } from '@/lib/MyStyles';

export default function ManageFunftPage(){
  return (
    <>
      <h1 className={BigHeading}>Manage fuNFTs</h1>

      <p className="mb-2">Here players can manage the fuNFTs they own. They can put them up for sale, take them off of sale, or just throw them away.</p>
    </>
  );
}

