import {BigHeading} from '@/lib/MyStyles';

export default function LoginPage(){
  return (
    <>
      <h1 className={BigHeading}>Login page</h1>
      <p>Users will log into the system here. It will redirect to the player Hub.</p>
      <p>The system will attempt to fetch credentials from browser storage and request a key otherwise.</p>
    </>
  );
}
