export default function AboutPage() {
  return (
    <>
      <h1 className="text-2xl text-center font-chemy">
        About the fuNFT game! 
      </h1>
      <p className="py-1">
	This page will include more information about the game, crypto in general, and me!
      </p>
    </>
  );
}
