import { BigHeading } from '@/lib/MyStyles';

export default function FeaturedPage(){
  return (
    <>
      <h1 className={BigHeading}>Featured Gallery</h1>

      <h2 className="text-base font-chemy">Guest Mode</h2>

      <p className="mb-2">Guests will see a list of featured fuNFTs for viewing entertainment along with estimated values based on bidding.</p>

      <h2 className="text-base font-chemy">Player Mode</h2>

      <p className="mb-2">Players who are logged in will see the same featured fuNFTs with the option to bid on each piece.</p>
    </>
  );
}

