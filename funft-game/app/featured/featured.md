# Featured Gallery

## Guest Mode

Guests will see a list of featured fuNFTs for viewing entertainment along with estimated values based on bidding.

## Player Mode

Players who are logged in will see the same featured fuNFTs with the option to bid on each piece.
