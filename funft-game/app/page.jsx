import Link from 'next/link';
import MyButton from '@/components/MyButton';

export default function HomePage() {
  return (
    <>
      <h1 className="text-2xl text-center font-chemy">
        fuNFT: NFT Simulation Game 
      </h1>
      <p className="py-1">
	Welcome to fuNFT central! Here you can create digital works of art called fuNFTs and sell them for funCoins. These pieces of art are simulated NFTs. As you play the game you will learn more about NFTs and cryptocurrency.
      </p>
	<p className="flex mt-1 justify-center w-full">
	  <Link href="signup"><MyButton>Join fuNFT</MyButton></Link>
	  <Link href="/about/"><MyButton>More Info </MyButton></Link>
      </p>
      <p>
	<Link href="/featured/">
	  Featured Gallery
	</Link>
      </p>
      <p>
        Ko-Fi Link
      </p>
    </>
  );
}
