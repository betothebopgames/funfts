import './globals.css';
import Logo from '@/components/logo-funft';
import Link from 'next/link';
import DropMenu from '@/components/dropmenu';

export const metadata = {
  title: 'Main Page',
  description: 'Generated by Next.js',
}

export default function RootLayout({ children }) {
  return (
      <html lang="en">
      <body className="bg-green-200 flex flex-col min-h-screen">
	<header>
	  <ul className="flex">
	    <li><Link href="/"><Logo /></Link></li>
	    <li className="m-1 ml-auto">
	      <DropMenu />
	    </li>
	  </ul>
	</header>
	<main className="grow m-1">
          {children}
	</main>
	<footer className="bg-green-800 text-green-100 p-1">
	  <h1 className="text-xl">fuNFT: NFT Simulation Game</h1>
	    <ul className="space-y-1 mt-1">
	      <li><Link href="/">Home</Link></li>
	      <li><Link href="/about">About</Link></li>
 	      <li><Link href="/featured">Featured fuNFTs</Link></li>
	      <li><Link href="/hub">Player Hub</Link></li>
	  </ul>
	</footer>
      </body>
    </html>
  )
}
