# Admin Panel

This admin-only page will be an easy way for me to moderate the site:

* Remove offensive fuNFTs
* Remove offensive messages
* Ban Player Accounts
* Approve new player accounts
* Add fuNFTs to Featured

