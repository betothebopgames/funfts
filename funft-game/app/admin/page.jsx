import {BigHeading} from '@/lib/MyStyles';

export default function AdminPage(){
  return (
    <>
<h1 className={BigHeading}>Admin Panel</h1>

<p className="mb-2">This admin-only page will be an easy way for me to moderate the site:</p>

<ul className="list-disc list-inside">
<li>Remove offensive fuNFTs</li>
<li>Remove offensive messages</li>
<li>Ban Player Accounts</li>
<li>Approve new player accounts</li>
<li>Add fuNFTs to Featured</li>
</ul>
    </>
  );
}
