import { BigHeading } from '@/lib/MyStyles';

export default function PlayerHub(){
  return (
    <>
      <h1 className={BigHeading}>Player Hub</h1>

<p>Shows the players game status and links to individual sections.</p>

<ul>
<li>Messages</li>
<li>Personal gallery</li>
<li>funCoin mining</li>
<li>Private collection</li>
<li>Users followed</li>
</ul>
    </>
  );
}

