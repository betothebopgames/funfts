import {BigHeading} from '@/lib/MyStyles';

export default function SignUpPage(){
  return (
    <>
      <h1 className={BigHeading}>Sign-up page</h1>
      <p>Players can register new accounts on this page.</p>
      <p>They will be automatically redirected to the login page on completion.</p>
    </>
  );
}
