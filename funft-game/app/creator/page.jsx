import { BigHeading } from '@/lib/MyStyles';

export default function CreatorPage(){
  return (
    <>
      <h1 className={BigHeading}>fuNFT Creator</h1>

      <p className="mb-2">This very interactive page will be where players design new fuNFTs to add to their collection.</p>
    </>
  );
}

