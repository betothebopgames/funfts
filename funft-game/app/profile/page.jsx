import { BigHeading } from '@/lib/MyStyles';

export default function ProfilePage(){
  return (
    <>
      <h1 className={BigHeading}>Player Profile</h1>

      <p className="mb-2">All players will have a profile page located underneath this directory. It will display all their fuNFTs and any awards.</p>
    </>
  );
}

