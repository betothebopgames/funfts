export default function Logo(){
    return(
	<div className="flex bg-white m-1 p-1.5 rounded-full font-chemy
	                  shadow shadow-green-600">
	  <span className="text-pink-400">f</span>
	  <span className="text-yellow-400">u</span>
	  <span className="text-red-600">N</span>
	  <span className="text-blue-800">F</span>
	  <span className="text-emerald-600">T</span>
      </div>
  );
}
