export default function MyButton({children}){
  return (
      <button type="button" className="flex p-1 m-1 bg-green-100 rounded-md 
	  shadow shadow-black hover:shadow-sm 
	  hover:shadow-green-800 active:shadow-none">
	{children}
      </button>
  );
}
