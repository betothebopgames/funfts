//Knex config

module.exports = {

  development: {
    client: 'sqlite3',
    connection: {
      filename: './db/data.db3'
    },
    migrations: {
      directory: './db/migrations'
    },
    seeds: {
      directory: './db/seeds'
    },
    useNullAsDefault: true
  }

};
