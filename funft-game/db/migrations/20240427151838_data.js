exports.up = async knex => {
  await knex.schema.createTable('users', table => {
    table.boolean('userID').unique().notNullable();
    table.text('name', 64);
    table.boolean('passkeyhash');
    table.text('email', 128);
  });
};

exports.down = async knex => {
  await knex.schema.dropTableIfExists('users');
};
