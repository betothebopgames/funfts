function replaceLetter(letter, reverse){
  let orig = "abcdefghijklmnopqrstuv";
  let enco = "abcdefghjkmnpqrtuvwxyz";
  for(let i = 0; i < 22; i++){
    if(orig.charAt(i) == letter.charAt(0) && !(reverse)){
      letter = enco.charAt(i);
      break;
    } else if (enco.charAt(i) == letter.charAt(0) && reverse){
      letter = orig.charAt(i);
      break;
    }
  }
  return letter;
}

function replaceChar(word, letter, index){
  let a = word.slice(0,index);
  let c = word.slice(index+1, word.length);
  return a + letter + c;
}

export function toBase32(num){
  let text = num.toString(32);
  for (let i = 0; i < text.length; i++){
    text = replaceChar(text, replaceLetter(text.charAt(i), false), i)
  }
  return text;
}

export function toNumber(text){
  for (let i = 0; i < text.length; i++){
    text = replaceChar(text, replaceLetter(text.charAt(i), true), i)
  }
  let num = parseInt(text, 32);
  return num;
}


console.log("testing");
/*for (let i = 0; i < (32 * 32 * 32); i++){
  console.log(toBase32(i));
}*/
let testvar = ["3","a","j","z","zz","10"];
testvar.forEach((example) => {console.log(toNumber(example));})
