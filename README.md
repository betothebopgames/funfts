# fuNFT: NFT Simulation Game

## Summary

What is a (FUN)FT? These Fantastic Unique Nostalgic Fictional Treasures are small pieces of digital art that are "securely" stored on this server. They can be purchased with FUNcoins that you can earn right here on the site. Can you collect the most FUNFT's? More than your friends?

This is an online game poking a little fun at the idea of NFT's or Non-Fungible Tokens, the digital assets that some people invest a lot of money into. fuNFT's unlike real NFT's will not be stored on any blockchain tecchnology and are unlikely to be worth any real world value. Players will be able to earn digital currency on the site and then spend it on buying different FUNFT's or earn money by creating their own.

FUNFT's will more specifically be an image with a small resolution (16 x 16), a limited color palette (16 colors), which can be represented with encoded text (Base64 or Base32). Each one should be a very small size, 176 bytes, with these settings.

Players will earn FUNcoins by creating their own FUNFT's. They can put these FUNFT's on sale in their personal galleries. Sometimes random NPC's, non-player characters, will buy these FUNFT's. Also, contests can be held where players can submit their FUNFT's and win prizes. They can also get FUNcoins if other players buy their FUNFTs. The site will also have a Featured Gallery where everybody on the site can view and purchase these highly valued FUNFT's in online auction format.

The site will be created using NextJS. I plan on storing all the data on the server using a sqlite database and Prisma ORM.

## Links
* [Taiga Project Planning Board](https://tree.taiga.io/project/thebebopcowboy-funft-nft-simulation-game/kanban)
* [Current Deployment]()
* [About Me]()
 
